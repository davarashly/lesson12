public class DateOfMeet {
    private String month;
    private String dayOfWeek;
    private String name;
    private String description;
    private int hours;
    private int minutes;
    private int day;

    public DateOfMeet(int day, String dayOfWeek, String month, int hours, int minutes) {
        this.dayOfWeek = dayOfWeek;
        this.day = day;
        this.month = month;
        this.name = "";
        this.description = "";
        this.hours = hours;
        this.minutes = minutes;
    }

    public DateOfMeet(int day, String dayOfWeek, String month, String name, int hours, int minutes) {
        this.dayOfWeek = dayOfWeek;
        this.day = day;
        this.month = month;
        this.name = name;
        this.description = Description.valueOf(name.toUpperCase()).description;
        this.hours = hours;
        this.minutes = minutes;
    }

    void show() {
        String s = "";
        if (!name.equals(""))
            s = "\nOn the " + dayOfWeek + ", " + day + " of " + month + " at " + hours + ":" + minutes + " will be " + name + ", " + description + ".";
        else if (hours == -1)
            s = "\nMeet is on the " + dayOfWeek + ", " + day + " of " + month + ".";
        else
            s = "\nMeet is on the " + dayOfWeek + ", " + day + " of " + month + " at " + hours + ":" + minutes + ".";

        System.out.print(s);

    }

    void setMeetNameDesc(String name, String description) {
        this.name = name;
        this.description = description;
    }

}

enum Day {
    MONDAY("monday"),
    TUESDAY("tuesday"),
    WEDNESDAY("wednesday"),
    THURSDAY("thursday"),
    FRIDAY("friday"),
    SATURDAY("saturday"),
    SUNDAY("sunday");

    public final String day;

    Day(String day) {
        this.day = day;
    }
}

enum Month {
    JANUARY("january"),
    FEBRUARY("february"),
    MARCH("march"),
    APRIL("april"),
    MAY("may"),
    JUNE("june"),
    JULY("july"),
    AUGUST("august"),
    SEPTEMBER("september"),
    OCTOBER("october"),
    NOVEMBER("november"),
    DECEMBER("december");

    public final String month;

    Month(String month) {
        this.month = month;
    }

}

enum Name {
    DATE("date"),
    FRIENDMEETING("friendmeeting"),
    MOVIE("movie"),
    BASKETBALL("basketball");

    public final String name;

    Name(String name) {
        this.name = name;
    }

}

enum Description {
    DATE("its so sweet date! Ouuwww"),
    FRIENDMEETING("its nice day for good friends to meet!"),
    MOVIE("gonna watch movie tonight!"),
    BASKETBALL("its ball time! Beware of this dribbling Ouuu");

    public final String description;

    Description(String description) {
        this.description = description;
    }

}