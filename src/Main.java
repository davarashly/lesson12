import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random rnd = new Random();

        DateOfMeet[] dates = new DateOfMeet[5];
        Day[] days = Day.values();
        Month[] months = Month.values();
        Name[] names = Name.values();

        for (int i = 0; i < dates.length; i++) {
            dates[i] = new DateOfMeet(rnd(0, 30), firstLetterUpperCase(days[rnd.nextInt(days.length)].day), firstLetterUpperCase(months[rnd.nextInt(months.length)].month), names[rnd.nextInt(names.length)].name, rnd(0, 23), rnd(0, 59));
            dates[i].show();
            System.out.println();
        }
        System.out.println("——————————————————————\n\n");

        BookOfMeeting calendar = new BookOfMeeting();
        calendar.printDates();



    }

    static String firstLetterUpperCase(String s){
        return s.substring(0,1).toUpperCase()+ s.substring(1);
    }

    static int rnd(int min, int max) {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }
}
