public class BookOfMeeting {
    DateOfMeet[] dates = createDates();
    int daycounter = 0;
    int days = 1;
    int i = 0;

    DateOfMeet[] createDates() {
        DateOfMeet[] dates_tmp = new DateOfMeet[365];
        int sum = 0;
        for (int j = 0; j < Month.values().length; j++) {
            if (j == 1)
                days = 28;
            else if ((j + 1) % 2 == 0 && j != 7)
                days = 30;
            else
                days = 31;
            for (int k = 1; k <= days; k++) {
                dates_tmp[i] = new DateOfMeet(k, Day.values()[daycounter].day, Month.values()[j].month, -1, -1);
                i++;
                if (daycounter == 6)
                    daycounter = 0;
                else
                    daycounter++;
            }
        }
        return dates_tmp;
    }

    void printDates() {
        for (int i = 0; i < dates.length; i++) {
            dates[i].show();
        }
    }

}
